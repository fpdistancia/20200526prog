package examen.ejercicio2;

import java.util.Date;

public class Asalariado extends Empleado {

    private static final long serialVersionUID = 1L;
    private String departamento;

    public Asalariado(String nombre, String apellidos, Date fechaContrato, 
            float salario, String cuentaBancaria, String departamento) {
        super(nombre, apellidos, fechaContrato, salario, cuentaBancaria);
        this.departamento = departamento;
    }

    public String getDepartamento() {
        return departamento;
    }

    @Override
    public String toString() {
        return String.format("Asalariado [%s]", super.toString());
    }
	
    @Override
    public void cobrar() {
        System.out.printf("Ingreso %.2f € en concepto de salario del asalariado %s - %tb\n",
                getSalario(), getNombre() + " " + getApellidos(), new Date());
    }

    /*
     * Generado por Eclipse
     */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((departamento == null) ? 0 : departamento.hashCode());
		return result;
	}

	/*
     * Generado por Eclipse
     */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		Asalariado other = (Asalariado) obj;
		if (departamento == null) {
			if (other.departamento != null)
				return false;
		} else if (!departamento.equals(other.departamento))
			return false;
		return true;
	}
	
}
